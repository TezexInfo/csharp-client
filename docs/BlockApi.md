# Info.Tezex.Api.BlockApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**BlocksAll**](BlockApi.md#blocksall) | **GET** /blocks/all | Get All Blocks 
[**BlocksByLevel**](BlockApi.md#blocksbylevel) | **GET** /blocks/{level} | Get All Blocks for a specific Level
[**BlocksByLevelRange**](BlockApi.md#blocksbylevelrange) | **GET** /blocks/{startlevel}/{stoplevel} | Get All Blocks for a specific Level-Range
[**GetBlock**](BlockApi.md#getblock) | **GET** /block/{blockhash} | Get Block By Blockhash
[**GetBlockDelegations**](BlockApi.md#getblockdelegations) | **GET** /block/{blockhash}/operations/delegations | Get Delegations of a Block
[**GetBlockEndorsements**](BlockApi.md#getblockendorsements) | **GET** /block/{blockhash}/operations/endorsements | Get Endorsements of a Block
[**GetBlockOperationsSorted**](BlockApi.md#getblockoperationssorted) | **GET** /block/{blockhash}/operations | Get operations of a block, sorted
[**GetBlockOriginations**](BlockApi.md#getblockoriginations) | **GET** /block/{blockhash}/operations/originations | Get Originations of a Block
[**GetBlockTransaction**](BlockApi.md#getblocktransaction) | **GET** /block/{blockhash}/operations/transactions | Get Transactions of Block
[**RecentBlocks**](BlockApi.md#recentblocks) | **GET** /blocks/recent | returns the last 25 blocks


<a name="blocksall"></a>
# **BlocksAll**
> BlocksAll BlocksAll (decimal? page = null, string order = null, int? limit = null)

Get All Blocks 

Get all Blocks

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class BlocksAllExample
    {
        public void main()
        {
            
            var apiInstance = new BlockApi();
            var page = 3.4;  // decimal? | Pagination, 200 tx per page max (optional) 
            var order = order_example;  // string | ASC or DESC (optional) 
            var limit = 56;  // int? | Results per Page (optional) 

            try
            {
                // Get All Blocks 
                BlocksAll result = apiInstance.BlocksAll(page, order, limit);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockApi.BlocksAll: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **decimal?**| Pagination, 200 tx per page max | [optional] 
 **order** | **string**| ASC or DESC | [optional] 
 **limit** | **int?**| Results per Page | [optional] 

### Return type

[**BlocksAll**](BlocksAll.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="blocksbylevel"></a>
# **BlocksByLevel**
> List<Block> BlocksByLevel (decimal? level)

Get All Blocks for a specific Level

Get all Blocks for a specific Level

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class BlocksByLevelExample
    {
        public void main()
        {
            
            var apiInstance = new BlockApi();
            var level = 3.4;  // decimal? | The level of the Blocks to retrieve, includes abandoned

            try
            {
                // Get All Blocks for a specific Level
                List&lt;Block&gt; result = apiInstance.BlocksByLevel(level);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockApi.BlocksByLevel: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **level** | **decimal?**| The level of the Blocks to retrieve, includes abandoned | 

### Return type

[**List<Block>**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="blocksbylevelrange"></a>
# **BlocksByLevelRange**
> BlockRange BlocksByLevelRange (decimal? startlevel, decimal? stoplevel)

Get All Blocks for a specific Level-Range

Get all Blocks for a specific Level-Range

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class BlocksByLevelRangeExample
    {
        public void main()
        {
            
            var apiInstance = new BlockApi();
            var startlevel = 3.4;  // decimal? | lowest blocklevel to return
            var stoplevel = 3.4;  // decimal? | highest blocklevel to return

            try
            {
                // Get All Blocks for a specific Level-Range
                BlockRange result = apiInstance.BlocksByLevelRange(startlevel, stoplevel);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockApi.BlocksByLevelRange: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **decimal?**| lowest blocklevel to return | 
 **stoplevel** | **decimal?**| highest blocklevel to return | 

### Return type

[**BlockRange**](BlockRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getblock"></a>
# **GetBlock**
> Block GetBlock (string blockhash)

Get Block By Blockhash

Get a block by its hash

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetBlockExample
    {
        public void main()
        {
            
            var apiInstance = new BlockApi();
            var blockhash = blockhash_example;  // string | The hash of the Block to retrieve

            try
            {
                // Get Block By Blockhash
                Block result = apiInstance.GetBlock(blockhash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockApi.GetBlock: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| The hash of the Block to retrieve | 

### Return type

[**Block**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getblockdelegations"></a>
# **GetBlockDelegations**
> List<Delegation> GetBlockDelegations (string blockhash)

Get Delegations of a Block

Get all Delegations of a specific Block

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetBlockDelegationsExample
    {
        public void main()
        {
            
            var apiInstance = new BlockApi();
            var blockhash = blockhash_example;  // string | Blockhash

            try
            {
                // Get Delegations of a Block
                List&lt;Delegation&gt; result = apiInstance.GetBlockDelegations(blockhash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockApi.GetBlockDelegations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash | 

### Return type

[**List<Delegation>**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getblockendorsements"></a>
# **GetBlockEndorsements**
> List<Endorsement> GetBlockEndorsements (string blockhash)

Get Endorsements of a Block

Get all Endorsements of a specific Block

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetBlockEndorsementsExample
    {
        public void main()
        {
            
            var apiInstance = new BlockApi();
            var blockhash = blockhash_example;  // string | Blockhash

            try
            {
                // Get Endorsements of a Block
                List&lt;Endorsement&gt; result = apiInstance.GetBlockEndorsements(blockhash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockApi.GetBlockEndorsements: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash | 

### Return type

[**List<Endorsement>**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getblockoperationssorted"></a>
# **GetBlockOperationsSorted**
> BlockOperationsSorted GetBlockOperationsSorted (string blockhash)

Get operations of a block, sorted

Get the maximum Level we have seen, Blocks at this level may become abandoned Blocks later on

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetBlockOperationsSortedExample
    {
        public void main()
        {
            
            var apiInstance = new BlockApi();
            var blockhash = blockhash_example;  // string | The hash of the Block to retrieve

            try
            {
                // Get operations of a block, sorted
                BlockOperationsSorted result = apiInstance.GetBlockOperationsSorted(blockhash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockApi.GetBlockOperationsSorted: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| The hash of the Block to retrieve | 

### Return type

[**BlockOperationsSorted**](BlockOperationsSorted.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getblockoriginations"></a>
# **GetBlockOriginations**
> List<Origination> GetBlockOriginations (string blockhash)

Get Originations of a Block

Get all Originations of a spcific Block

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetBlockOriginationsExample
    {
        public void main()
        {
            
            var apiInstance = new BlockApi();
            var blockhash = blockhash_example;  // string | Blockhash

            try
            {
                // Get Originations of a Block
                List&lt;Origination&gt; result = apiInstance.GetBlockOriginations(blockhash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockApi.GetBlockOriginations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash | 

### Return type

[**List<Origination>**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getblocktransaction"></a>
# **GetBlockTransaction**
> List<Transaction> GetBlockTransaction (string blockhash)

Get Transactions of Block

Get all Transactions of a spcific Block

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetBlockTransactionExample
    {
        public void main()
        {
            
            var apiInstance = new BlockApi();
            var blockhash = blockhash_example;  // string | Blockhash

            try
            {
                // Get Transactions of Block
                List&lt;Transaction&gt; result = apiInstance.GetBlockTransaction(blockhash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockApi.GetBlockTransaction: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash | 

### Return type

[**List<Transaction>**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="recentblocks"></a>
# **RecentBlocks**
> List<Block> RecentBlocks ()

returns the last 25 blocks

Get all Blocks for a specific Level

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class RecentBlocksExample
    {
        public void main()
        {
            
            var apiInstance = new BlockApi();

            try
            {
                // returns the last 25 blocks
                List&lt;Block&gt; result = apiInstance.RecentBlocks();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockApi.RecentBlocks: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<Block>**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


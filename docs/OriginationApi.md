# Info.Tezex.Api.OriginationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetOrigination**](OriginationApi.md#getorigination) | **GET** /origination/{origination_hash} | Get Origination


<a name="getorigination"></a>
# **GetOrigination**
> Origination GetOrigination (string originationHash)

Get Origination

Get a specific Origination

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetOriginationExample
    {
        public void main()
        {
            
            var apiInstance = new OriginationApi();
            var originationHash = originationHash_example;  // string | The hash of the Origination to retrieve

            try
            {
                // Get Origination
                Origination result = apiInstance.GetOrigination(originationHash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OriginationApi.GetOrigination: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **originationHash** | **string**| The hash of the Origination to retrieve | 

### Return type

[**Origination**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


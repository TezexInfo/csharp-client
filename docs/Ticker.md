# Info.Tezex.Model.Ticker
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BTC** | **string** |  | [optional] 
**CNY** | **string** |  | [optional] 
**USD** | **string** |  | [optional] 
**EUR** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


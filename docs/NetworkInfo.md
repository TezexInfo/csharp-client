# Info.Tezex.Model.NetworkInfo
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MaxLevel** | **int?** |  | [optional] 
**Blocktime** | **string** |  | [optional] 
**Transactions24h** | **int?** |  | [optional] 
**Oeprations24h** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


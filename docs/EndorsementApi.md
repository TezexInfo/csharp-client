# Info.Tezex.Api.EndorsementApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetEndorsement**](EndorsementApi.md#getendorsement) | **GET** /endorsement/{endorsement_hash} | Get Endorsement
[**GetEndorsementForBlock**](EndorsementApi.md#getendorsementforblock) | **GET** /endorsement/for/{block_hash} | Get Endorsement


<a name="getendorsement"></a>
# **GetEndorsement**
> Endorsement GetEndorsement (string endorsementHash)

Get Endorsement

Get a specific Endorsement

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetEndorsementExample
    {
        public void main()
        {
            
            var apiInstance = new EndorsementApi();
            var endorsementHash = endorsementHash_example;  // string | The hash of the Endorsement to retrieve

            try
            {
                // Get Endorsement
                Endorsement result = apiInstance.GetEndorsement(endorsementHash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EndorsementApi.GetEndorsement: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **endorsementHash** | **string**| The hash of the Endorsement to retrieve | 

### Return type

[**Endorsement**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getendorsementforblock"></a>
# **GetEndorsementForBlock**
> List<Endorsement> GetEndorsementForBlock (string blockHash)

Get Endorsement

Get a specific Endorsement

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetEndorsementForBlockExample
    {
        public void main()
        {
            
            var apiInstance = new EndorsementApi();
            var blockHash = blockHash_example;  // string | blockhash

            try
            {
                // Get Endorsement
                List&lt;Endorsement&gt; result = apiInstance.GetEndorsementForBlock(blockHash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EndorsementApi.GetEndorsementForBlock: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockHash** | **string**| blockhash | 

### Return type

[**List<Endorsement>**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


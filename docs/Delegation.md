# Info.Tezex.Model.Delegation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hash** | **string** |  | [optional] 
**Branch** | **string** |  | [optional] 
**Source** | **string** |  | [optional] 
**PublicKey** | **string** |  | [optional] 
**Fee** | **int?** |  | [optional] 
**Counter** | **int?** |  | [optional] 
**_Delegate** | **string** |  | [optional] 
**Level** | **int?** |  | [optional] 
**BlockHash** | **string** |  | [optional] 
**Time** | **DateTime?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


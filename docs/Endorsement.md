# Info.Tezex.Model.Endorsement
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hash** | **string** |  | [optional] 
**Source** | **string** |  | [optional] 
**Level** | **int?** |  | [optional] 
**Block** | **string** |  | [optional] 
**EndorsedBlock** | **string** |  | [optional] 
**Slot** | **int?** |  | [optional] 
**Time** | **DateTime?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# Info.Tezex.Api.AccountApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAccount**](AccountApi.md#getaccount) | **GET** /account/{account} | Get Account
[**GetAccountBalance**](AccountApi.md#getaccountbalance) | **GET** /account/{account}/balance | Get Account Balance
[**GetAccountLastSeen**](AccountApi.md#getaccountlastseen) | **GET** /account/{account}/last_seen | Get last active date
[**GetAccountOperationCount**](AccountApi.md#getaccountoperationcount) | **GET** /account/{account}/operations_count | Get operation count of Account
[**GetAccountTransactionCount**](AccountApi.md#getaccounttransactioncount) | **GET** /account/{account}/transaction_count | Get transaction count of Account
[**GetDelegationsForAccount**](AccountApi.md#getdelegationsforaccount) | **GET** /account/{account}/delegations | Get Delegations of this account
[**GetDelegationsToAccount**](AccountApi.md#getdelegationstoaccount) | **GET** /account/{account}/delegated | Get Delegations to this account
[**GetEndorsementsForAccount**](AccountApi.md#getendorsementsforaccount) | **GET** /account/{account}/endorsements | Get Endorsements this Account has made
[**GetTransactionForAccountIncoming**](AccountApi.md#gettransactionforaccountincoming) | **GET** /account/{account}/transactions/incoming | Get Transaction
[**GetTransactionForAccountOutgoing**](AccountApi.md#gettransactionforaccountoutgoing) | **GET** /account/{account}/transactions/outgoing | Get Transaction


<a name="getaccount"></a>
# **GetAccount**
> Account GetAccount (string account)

Get Account

Get Acccount

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetAccountExample
    {
        public void main()
        {
            
            var apiInstance = new AccountApi();
            var account = account_example;  // string | The account

            try
            {
                // Get Account
                Account result = apiInstance.GetAccount(account);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountApi.GetAccount: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getaccountbalance"></a>
# **GetAccountBalance**
> string GetAccountBalance (string account)

Get Account Balance

Get Balance

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetAccountBalanceExample
    {
        public void main()
        {
            
            var apiInstance = new AccountApi();
            var account = account_example;  // string | The account

            try
            {
                // Get Account Balance
                string result = apiInstance.GetAccountBalance(account);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountApi.GetAccountBalance: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account | 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getaccountlastseen"></a>
# **GetAccountLastSeen**
> DateTime? GetAccountLastSeen (string account)

Get last active date

Get LastSeen Date

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetAccountLastSeenExample
    {
        public void main()
        {
            
            var apiInstance = new AccountApi();
            var account = account_example;  // string | The account

            try
            {
                // Get last active date
                DateTime? result = apiInstance.GetAccountLastSeen(account);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountApi.GetAccountLastSeen: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account | 

### Return type

**DateTime?**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getaccountoperationcount"></a>
# **GetAccountOperationCount**
> int? GetAccountOperationCount (string account)

Get operation count of Account

Get Operation Count

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetAccountOperationCountExample
    {
        public void main()
        {
            
            var apiInstance = new AccountApi();
            var account = account_example;  // string | The account

            try
            {
                // Get operation count of Account
                int? result = apiInstance.GetAccountOperationCount(account);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountApi.GetAccountOperationCount: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account | 

### Return type

**int?**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getaccounttransactioncount"></a>
# **GetAccountTransactionCount**
> int? GetAccountTransactionCount (string account)

Get transaction count of Account

Get Transaction Count

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetAccountTransactionCountExample
    {
        public void main()
        {
            
            var apiInstance = new AccountApi();
            var account = account_example;  // string | The account

            try
            {
                // Get transaction count of Account
                int? result = apiInstance.GetAccountTransactionCount(account);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountApi.GetAccountTransactionCount: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account | 

### Return type

**int?**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdelegationsforaccount"></a>
# **GetDelegationsForAccount**
> List<Delegation> GetDelegationsForAccount (string account, int? before = null)

Get Delegations of this account

Get Delegations this Account has made

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetDelegationsForAccountExample
    {
        public void main()
        {
            
            var apiInstance = new AccountApi();
            var account = account_example;  // string | The account for which to retrieve Delegations
            var before = 56;  // int? | Only Return Delegations before this blocklevel (optional) 

            try
            {
                // Get Delegations of this account
                List&lt;Delegation&gt; result = apiInstance.GetDelegationsForAccount(account, before);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountApi.GetDelegationsForAccount: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve Delegations | 
 **before** | **int?**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**List<Delegation>**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdelegationstoaccount"></a>
# **GetDelegationsToAccount**
> List<Delegation> GetDelegationsToAccount (string account, int? before = null)

Get Delegations to this account

Get that have been made to this Account

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetDelegationsToAccountExample
    {
        public void main()
        {
            
            var apiInstance = new AccountApi();
            var account = account_example;  // string | The account to which delegations have been made
            var before = 56;  // int? | Only Return Delegations before this blocklevel (optional) 

            try
            {
                // Get Delegations to this account
                List&lt;Delegation&gt; result = apiInstance.GetDelegationsToAccount(account, before);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountApi.GetDelegationsToAccount: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account to which delegations have been made | 
 **before** | **int?**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**List<Delegation>**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getendorsementsforaccount"></a>
# **GetEndorsementsForAccount**
> List<Endorsement> GetEndorsementsForAccount (string account, int? before = null)

Get Endorsements this Account has made

Get Endorsements this Account has made

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetEndorsementsForAccountExample
    {
        public void main()
        {
            
            var apiInstance = new AccountApi();
            var account = account_example;  // string | The account for which to retrieve Endorsements
            var before = 56;  // int? | Only Return Delegations before this blocklevel (optional) 

            try
            {
                // Get Endorsements this Account has made
                List&lt;Endorsement&gt; result = apiInstance.GetEndorsementsForAccount(account, before);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountApi.GetEndorsementsForAccount: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve Endorsements | 
 **before** | **int?**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**List<Endorsement>**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettransactionforaccountincoming"></a>
# **GetTransactionForAccountIncoming**
> Transactions GetTransactionForAccountIncoming (string account, int? before = null)

Get Transaction

Get incoming Transactions for a specific Account

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetTransactionForAccountIncomingExample
    {
        public void main()
        {
            
            var apiInstance = new AccountApi();
            var account = account_example;  // string | The account for which to retrieve incoming Transactions
            var before = 56;  // int? | Only Return transactions before this blocklevel (optional) 

            try
            {
                // Get Transaction
                Transactions result = apiInstance.GetTransactionForAccountIncoming(account, before);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountApi.GetTransactionForAccountIncoming: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve incoming Transactions | 
 **before** | **int?**| Only Return transactions before this blocklevel | [optional] 

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettransactionforaccountoutgoing"></a>
# **GetTransactionForAccountOutgoing**
> Transactions GetTransactionForAccountOutgoing (string account, int? before = null)

Get Transaction

Get outgoing Transactions for a specific Account

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetTransactionForAccountOutgoingExample
    {
        public void main()
        {
            
            var apiInstance = new AccountApi();
            var account = account_example;  // string | The account for which to retrieve outgoing Transactions
            var before = 56;  // int? | Only return transactions before this blocklevel (optional) 

            try
            {
                // Get Transaction
                Transactions result = apiInstance.GetTransactionForAccountOutgoing(account, before);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountApi.GetTransactionForAccountOutgoing: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve outgoing Transactions | 
 **before** | **int?**| Only return transactions before this blocklevel | [optional] 

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# Info.Tezex.Model.StatsOverview
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PriceUsd** | **string** |  | [optional] 
**PriceBtc** | **string** |  | [optional] 
**BlockTime** | **int?** | Blocktime in seconds | [optional] 
**Priority** | **decimal?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


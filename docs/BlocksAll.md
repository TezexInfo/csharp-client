# Info.Tezex.Model.BlocksAll
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MaxBlockLevel** | **int?** |  | [optional] 
**Blocks** | [**List&lt;Block&gt;**](Block.md) |  | [optional] 
**TotalResults** | **int?** |  | [optional] 
**CurrentPage** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


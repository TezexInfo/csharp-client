# Info.Tezex.Api.BlockchainApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Blockheight**](BlockchainApi.md#blockheight) | **GET** /maxLevel | Get Max Blockheight


<a name="blockheight"></a>
# **Blockheight**
> Level Blockheight ()

Get Max Blockheight

Get the maximum Level we have seen

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class BlockheightExample
    {
        public void main()
        {
            
            var apiInstance = new BlockchainApi();

            try
            {
                // Get Max Blockheight
                Level result = apiInstance.Blockheight();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BlockchainApi.Blockheight: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Level**](Level.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


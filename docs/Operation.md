# Info.Tezex.Model.Operation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Transaction** | [**Transaction**](Transaction.md) |  | [optional] 
**Origination** | [**Origination**](Origination.md) |  | [optional] 
**Delegation** | [**Delegation**](Delegation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# Info.Tezex.Model.Level
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_Level** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


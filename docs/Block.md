# Info.Tezex.Model.Block
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hash** | **string** |  | [optional] 
**NetId** | **string** |  | [optional] 
**Protocol** | **string** |  | [optional] 
**Level** | **int?** |  | [optional] 
**Proto** | **string** |  | [optional] 
**Successors** | [**List&lt;ChainStatus&gt;**](ChainStatus.md) |  | [optional] 
**Predecessor** | **string** |  | [optional] 
**Time** | **DateTime?** |  | [optional] 
**ValidationPass** | **string** |  | [optional] 
**Data** | **string** |  | [optional] 
**ChainStatus** | **string** |  | [optional] 
**OperationsCount** | **int?** |  | [optional] 
**OperationsHash** | **string** |  | [optional] 
**Baker** | **string** |  | [optional] 
**SeedNonceHash** | **string** |  | [optional] 
**ProofOfWorkNonce** | **string** |  | [optional] 
**Signature** | **string** |  | [optional] 
**Priority** | **int?** |  | [optional] 
**OperationCount** | **int?** |  | [optional] 
**TotalFee** | **string** |  | [optional] 
**Operations** | [**BlockOperationsSorted**](BlockOperationsSorted.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# Info.Tezex.Model.TransactionRange
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MaxBlockLevel** | **int?** |  | [optional] 
**Transactions** | [**List&lt;Transaction&gt;**](Transaction.md) |  | [optional] 
**TotalResults** | **int?** |  | [optional] 
**CurrentPage** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


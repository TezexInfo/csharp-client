# Info.Tezex.Api.NetworkApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Network**](NetworkApi.md#network) | **GET** /network | Get Network Information


<a name="network"></a>
# **Network**
> NetworkInfo Network ()

Get Network Information

Get Network Information

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class NetworkExample
    {
        public void main()
        {
            
            var apiInstance = new NetworkApi();

            try
            {
                // Get Network Information
                NetworkInfo result = apiInstance.Network();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling NetworkApi.Network: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NetworkInfo**](NetworkInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


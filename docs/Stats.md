# Info.Tezex.Model.Stats
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StatGroup** | **string** |  | [optional] 
**Stat** | **string** |  | [optional] 
**Value** | **string** |  | [optional] 
**Start** | **DateTime?** |  | [optional] 
**End** | **DateTime?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


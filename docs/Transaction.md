# Info.Tezex.Model.Transaction
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hash** | **string** |  | [optional] 
**Branch** | **string** |  | [optional] 
**Source** | **string** |  | [optional] 
**PublicKey** | **string** |  | [optional] 
**Level** | **int?** |  | [optional] 
**BlockHash** | **string** |  | [optional] 
**Counter** | **int?** |  | [optional] 
**Time** | **DateTime?** |  | [optional] 
**Operations** | [**List&lt;TransactionOperation&gt;**](TransactionOperation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


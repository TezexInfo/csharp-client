# Info.Tezex.Model.Origination
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hash** | **string** |  | [optional] 
**Branch** | **string** |  | [optional] 
**Source** | **string** |  | [optional] 
**PublicKey** | **string** |  | [optional] 
**Fee** | **int?** |  | [optional] 
**Counter** | **int?** |  | [optional] 
**Operations** | [**List&lt;OriginationOperation&gt;**](OriginationOperation.md) |  | [optional] 
**Level** | **int?** |  | [optional] 
**BlockHash** | **string** |  | [optional] 
**Time** | **DateTime?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


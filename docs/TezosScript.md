# Info.Tezex.Model.TezosScript
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_Int** | **string** |  | [optional] 
**_String** | **string** |  | [optional] 
**Prim** | **string** |  | [optional] 
**Args** | [**List&lt;TezosScript&gt;**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# Info.Tezex.Model.BlockOperationsSorted
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Transactions** | [**List&lt;Transaction&gt;**](Transaction.md) |  | [optional] 
**Originations** | [**List&lt;Origination&gt;**](Origination.md) |  | [optional] 
**Delegations** | [**List&lt;Delegation&gt;**](Delegation.md) |  | [optional] 
**Endorsements** | [**List&lt;Endorsement&gt;**](Endorsement.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# Info.Tezex.Api.DelegationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetDelegation**](DelegationApi.md#getdelegation) | **GET** /delegation/{delegation_hash} | Get Delegation


<a name="getdelegation"></a>
# **GetDelegation**
> Delegation GetDelegation (string delegationHash)

Get Delegation

Get a specific Delegation

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetDelegationExample
    {
        public void main()
        {
            
            var apiInstance = new DelegationApi();
            var delegationHash = delegationHash_example;  // string | The hash of the Origination to retrieve

            try
            {
                // Get Delegation
                Delegation result = apiInstance.GetDelegation(delegationHash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DelegationApi.GetDelegation: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegationHash** | **string**| The hash of the Origination to retrieve | 

### Return type

[**Delegation**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


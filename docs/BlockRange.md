# Info.Tezex.Model.BlockRange
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MaxBlockLevel** | **int?** |  | [optional] 
**Blocks** | [**List&lt;Block&gt;**](Block.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


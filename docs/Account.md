# Info.Tezex.Model.Account
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Address** | **string** |  | [optional] 
**OperationCount** | **int?** |  | [optional] 
**SentTransactionCount** | **int?** |  | [optional] 
**RecvTransactionCount** | **int?** |  | [optional] 
**OriginationCount** | **int?** |  | [optional] 
**DelegationCount** | **int?** |  | [optional] 
**DelegatedCount** | **int?** |  | [optional] 
**EndorsementCount** | **int?** |  | [optional] 
**FirstSeen** | **DateTime?** |  | [optional] 
**LastSeen** | **DateTime?** |  | [optional] 
**Name** | **string** |  | [optional] 
**Balance** | **string** |  | [optional] 
**TotalSent** | **string** |  | [optional] 
**TotalReceived** | **string** |  | [optional] 
**BakedBlocks** | **int?** |  | [optional] 
**ImageUrl** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


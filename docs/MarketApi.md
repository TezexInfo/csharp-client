# Info.Tezex.Api.MarketApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Candlestick**](MarketApi.md#candlestick) | **GET** /price/{denominator}/{numerator}/{period} | Candlestick Data
[**Ticker**](MarketApi.md#ticker) | **GET** /ticker/{numerator} | Get Ticker for a specific Currency


<a name="candlestick"></a>
# **Candlestick**
> List<Candlestick> Candlestick (string denominator, string numerator, string period)

Candlestick Data

Returns CandleStick Prices

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class CandlestickExample
    {
        public void main()
        {
            
            var apiInstance = new MarketApi();
            var denominator = denominator_example;  // string | which currency
            var numerator = numerator_example;  // string | to which currency
            var period = period_example;  // string | Timeframe of one candle

            try
            {
                // Candlestick Data
                List&lt;Candlestick&gt; result = apiInstance.Candlestick(denominator, numerator, period);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MarketApi.Candlestick: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **denominator** | **string**| which currency | 
 **numerator** | **string**| to which currency | 
 **period** | **string**| Timeframe of one candle | 

### Return type

[**List<Candlestick>**](Candlestick.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="ticker"></a>
# **Ticker**
> Ticker Ticker (string numerator)

Get Ticker for a specific Currency

Returns BTC, USD, EUR and CNY Prices

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class TickerExample
    {
        public void main()
        {
            
            var apiInstance = new MarketApi();
            var numerator = numerator_example;  // string | The level of the Blocks to retrieve

            try
            {
                // Get Ticker for a specific Currency
                Ticker result = apiInstance.Ticker(numerator);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MarketApi.Ticker: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numerator** | **string**| The level of the Blocks to retrieve | 

### Return type

[**Ticker**](Ticker.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


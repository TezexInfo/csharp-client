# Info.Tezex.Model.OriginationOperation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Kind** | **string** |  | [optional] 
**ManagerPubkey** | **string** |  | [optional] 
**Balance** | **int?** |  | [optional] 
**Spendable** | **bool?** |  | [optional] 
**Delegateable** | **bool?** |  | [optional] 
**_Delegate** | **string** |  | [optional] 
**Script** | [**TezosScript**](TezosScript.md) |  | [optional] 
**Storage** | [**TezosScript**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


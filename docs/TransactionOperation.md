# Info.Tezex.Model.TransactionOperation
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Kind** | **string** |  | [optional] 
**Amount** | **int?** |  | [optional] 
**Destination** | **string** |  | [optional] 
**Parameters** | [**TezosScript**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


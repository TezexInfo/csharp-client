# Info.Tezex.Api.TransactionApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetTransaction**](TransactionApi.md#gettransaction) | **GET** /transaction/{transaction_hash} | Get Transaction
[**GetTransactionsRecent**](TransactionApi.md#gettransactionsrecent) | **GET** /transactions/recent | Returns the last 50 Transactions
[**TransactionsAll**](TransactionApi.md#transactionsall) | **GET** /transactions/all | Get All Transactions
[**TransactionsByLevelRange**](TransactionApi.md#transactionsbylevelrange) | **GET** /transactions/{startlevel}/{stoplevel} | Get All Transactions for a specific Level-Range


<a name="gettransaction"></a>
# **GetTransaction**
> Transaction GetTransaction (string transactionHash)

Get Transaction

Get a specific Transaction

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetTransactionExample
    {
        public void main()
        {
            
            var apiInstance = new TransactionApi();
            var transactionHash = transactionHash_example;  // string | The hash of the Transaction to retrieve

            try
            {
                // Get Transaction
                Transaction result = apiInstance.GetTransaction(transactionHash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TransactionApi.GetTransaction: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transactionHash** | **string**| The hash of the Transaction to retrieve | 

### Return type

[**Transaction**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettransactionsrecent"></a>
# **GetTransactionsRecent**
> List<Transaction> GetTransactionsRecent ()

Returns the last 50 Transactions

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetTransactionsRecentExample
    {
        public void main()
        {
            
            var apiInstance = new TransactionApi();

            try
            {
                // Returns the last 50 Transactions
                List&lt;Transaction&gt; result = apiInstance.GetTransactionsRecent();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TransactionApi.GetTransactionsRecent: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<Transaction>**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="transactionsall"></a>
# **TransactionsAll**
> TransactionRange TransactionsAll (decimal? page = null, string order = null, int? limit = null)

Get All Transactions

Get all Transactions

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class TransactionsAllExample
    {
        public void main()
        {
            
            var apiInstance = new TransactionApi();
            var page = 3.4;  // decimal? | Pagination, 200 tx per page max (optional) 
            var order = order_example;  // string | ASC or DESC (optional) 
            var limit = 56;  // int? | Results per Page (optional) 

            try
            {
                // Get All Transactions
                TransactionRange result = apiInstance.TransactionsAll(page, order, limit);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TransactionApi.TransactionsAll: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **decimal?**| Pagination, 200 tx per page max | [optional] 
 **order** | **string**| ASC or DESC | [optional] 
 **limit** | **int?**| Results per Page | [optional] 

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="transactionsbylevelrange"></a>
# **TransactionsByLevelRange**
> TransactionRange TransactionsByLevelRange (decimal? startlevel, decimal? stoplevel, decimal? page = null, string order = null, int? limit = null)

Get All Transactions for a specific Level-Range

Get all Transactions for a specific Level-Range

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class TransactionsByLevelRangeExample
    {
        public void main()
        {
            
            var apiInstance = new TransactionApi();
            var startlevel = 3.4;  // decimal? | lowest blocklevel to return
            var stoplevel = 3.4;  // decimal? | highest blocklevel to return
            var page = 3.4;  // decimal? | Pagination, 200 tx per page max (optional) 
            var order = order_example;  // string | ASC or DESC (optional) 
            var limit = 56;  // int? | Results per Page (optional) 

            try
            {
                // Get All Transactions for a specific Level-Range
                TransactionRange result = apiInstance.TransactionsByLevelRange(startlevel, stoplevel, page, order, limit);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TransactionApi.TransactionsByLevelRange: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **decimal?**| lowest blocklevel to return | 
 **stoplevel** | **decimal?**| highest blocklevel to return | 
 **page** | **decimal?**| Pagination, 200 tx per page max | [optional] 
 **order** | **string**| ASC or DESC | [optional] 
 **limit** | **int?**| Results per Page | [optional] 

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


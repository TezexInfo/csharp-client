# Info.Tezex.Api.OperationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetOperation**](OperationApi.md#getoperation) | **GET** /operation/{operation_hash} | Get Operation


<a name="getoperation"></a>
# **GetOperation**
> Operation GetOperation (string operationHash)

Get Operation

Get a specific Operation

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetOperationExample
    {
        public void main()
        {
            
            var apiInstance = new OperationApi();
            var operationHash = operationHash_example;  // string | The hash of the Operation to retrieve

            try
            {
                // Get Operation
                Operation result = apiInstance.GetOperation(operationHash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OperationApi.GetOperation: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **operationHash** | **string**| The hash of the Operation to retrieve | 

### Return type

[**Operation**](Operation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


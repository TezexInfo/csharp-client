# Info.Tezex.Model.Candlestick
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**O** | **string** |  | [optional] 
**H** | **string** |  | [optional] 
**L** | **string** |  | [optional] 
**C** | **string** |  | [optional] 
**Vol** | **string** |  | [optional] 
**Time** | **DateTime?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


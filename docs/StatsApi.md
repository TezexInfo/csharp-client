# Info.Tezex.Api.StatsApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetStatistics**](StatsApi.md#getstatistics) | **GET** /stats/{group}/{stat}/{period} | Get Statistics
[**GetStatsOverview**](StatsApi.md#getstatsoverview) | **GET** /stats/overview | Returns some basic Info


<a name="getstatistics"></a>
# **GetStatistics**
> List<Stats> GetStatistics (string group, string stat, string period, DateTime? startTime = null, DateTime? endTime = null)

Get Statistics

Get Statistics

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetStatisticsExample
    {
        public void main()
        {
            
            var apiInstance = new StatsApi();
            var group = group_example;  // string | Block, Transaction, etc
            var stat = stat_example;  // string | 
            var period = period_example;  // string | 
            var startTime = 2013-10-20T19:20:30+01:00;  // DateTime? |  (optional) 
            var endTime = 2013-10-20T19:20:30+01:00;  // DateTime? |  (optional) 

            try
            {
                // Get Statistics
                List&lt;Stats&gt; result = apiInstance.GetStatistics(group, stat, period, startTime, endTime);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling StatsApi.GetStatistics: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **string**| Block, Transaction, etc | 
 **stat** | **string**|  | 
 **period** | **string**|  | 
 **startTime** | **DateTime?**|  | [optional] 
 **endTime** | **DateTime?**|  | [optional] 

### Return type

[**List<Stats>**](Stats.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getstatsoverview"></a>
# **GetStatsOverview**
> StatsOverview GetStatsOverview ()

Returns some basic Info

### Example
```csharp
using System;
using System.Diagnostics;
using Info.Tezex.Api;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Example
{
    public class GetStatsOverviewExample
    {
        public void main()
        {
            
            var apiInstance = new StatsApi();

            try
            {
                // Returns some basic Info
                StatsOverview result = apiInstance.GetStatsOverview();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling StatsApi.GetStatsOverview: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatsOverview**](StatsOverview.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


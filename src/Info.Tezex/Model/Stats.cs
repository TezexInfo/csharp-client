/* 
 * TezosAPI
 *
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Info.Tezex.Model
{
    /// <summary>
    /// See StatsDescription for all valid Endpoints
    /// </summary>
    [DataContract]
    public partial class Stats :  IEquatable<Stats>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Stats" /> class.
        /// </summary>
        /// <param name="StatGroup">StatGroup.</param>
        /// <param name="Stat">Stat.</param>
        /// <param name="Value">Value.</param>
        /// <param name="Start">Start.</param>
        /// <param name="End">End.</param>
        public Stats(string StatGroup = null, string Stat = null, string Value = null, DateTime? Start = null, DateTime? End = null)
        {
            this.StatGroup = StatGroup;
            this.Stat = Stat;
            this.Value = Value;
            this.Start = Start;
            this.End = End;
        }
        
        /// <summary>
        /// Gets or Sets StatGroup
        /// </summary>
        [DataMember(Name="stat_group", EmitDefaultValue=false)]
        public string StatGroup { get; set; }
        /// <summary>
        /// Gets or Sets Stat
        /// </summary>
        [DataMember(Name="stat", EmitDefaultValue=false)]
        public string Stat { get; set; }
        /// <summary>
        /// Gets or Sets Value
        /// </summary>
        [DataMember(Name="value", EmitDefaultValue=false)]
        public string Value { get; set; }
        /// <summary>
        /// Gets or Sets Start
        /// </summary>
        [DataMember(Name="start", EmitDefaultValue=false)]
        public DateTime? Start { get; set; }
        /// <summary>
        /// Gets or Sets End
        /// </summary>
        [DataMember(Name="end", EmitDefaultValue=false)]
        public DateTime? End { get; set; }
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Stats {\n");
            sb.Append("  StatGroup: ").Append(StatGroup).Append("\n");
            sb.Append("  Stat: ").Append(Stat).Append("\n");
            sb.Append("  Value: ").Append(Value).Append("\n");
            sb.Append("  Start: ").Append(Start).Append("\n");
            sb.Append("  End: ").Append(End).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as Stats);
        }

        /// <summary>
        /// Returns true if Stats instances are equal
        /// </summary>
        /// <param name="other">Instance of Stats to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Stats other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return 
                (
                    this.StatGroup == other.StatGroup ||
                    this.StatGroup != null &&
                    this.StatGroup.Equals(other.StatGroup)
                ) && 
                (
                    this.Stat == other.Stat ||
                    this.Stat != null &&
                    this.Stat.Equals(other.Stat)
                ) && 
                (
                    this.Value == other.Value ||
                    this.Value != null &&
                    this.Value.Equals(other.Value)
                ) && 
                (
                    this.Start == other.Start ||
                    this.Start != null &&
                    this.Start.Equals(other.Start)
                ) && 
                (
                    this.End == other.End ||
                    this.End != null &&
                    this.End.Equals(other.End)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.StatGroup != null)
                    hash = hash * 59 + this.StatGroup.GetHashCode();
                if (this.Stat != null)
                    hash = hash * 59 + this.Stat.GetHashCode();
                if (this.Value != null)
                    hash = hash * 59 + this.Value.GetHashCode();
                if (this.Start != null)
                    hash = hash * 59 + this.Start.GetHashCode();
                if (this.End != null)
                    hash = hash * 59 + this.End.GetHashCode();
                return hash;
            }
        }
    }

}

/* 
 * TezosAPI
 *
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Info.Tezex.Model
{
    /// <summary>
    /// BlockRange
    /// </summary>
    [DataContract]
    public partial class BlockRange :  IEquatable<BlockRange>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BlockRange" /> class.
        /// </summary>
        /// <param name="MaxBlockLevel">MaxBlockLevel.</param>
        /// <param name="Blocks">Blocks.</param>
        public BlockRange(int? MaxBlockLevel = null, List<Block> Blocks = null)
        {
            this.MaxBlockLevel = MaxBlockLevel;
            this.Blocks = Blocks;
        }
        
        /// <summary>
        /// Gets or Sets MaxBlockLevel
        /// </summary>
        [DataMember(Name="maxBlockLevel", EmitDefaultValue=false)]
        public int? MaxBlockLevel { get; set; }
        /// <summary>
        /// Gets or Sets Blocks
        /// </summary>
        [DataMember(Name="blocks", EmitDefaultValue=false)]
        public List<Block> Blocks { get; set; }
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class BlockRange {\n");
            sb.Append("  MaxBlockLevel: ").Append(MaxBlockLevel).Append("\n");
            sb.Append("  Blocks: ").Append(Blocks).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as BlockRange);
        }

        /// <summary>
        /// Returns true if BlockRange instances are equal
        /// </summary>
        /// <param name="other">Instance of BlockRange to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(BlockRange other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return 
                (
                    this.MaxBlockLevel == other.MaxBlockLevel ||
                    this.MaxBlockLevel != null &&
                    this.MaxBlockLevel.Equals(other.MaxBlockLevel)
                ) && 
                (
                    this.Blocks == other.Blocks ||
                    this.Blocks != null &&
                    this.Blocks.SequenceEqual(other.Blocks)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.MaxBlockLevel != null)
                    hash = hash * 59 + this.MaxBlockLevel.GetHashCode();
                if (this.Blocks != null)
                    hash = hash * 59 + this.Blocks.GetHashCode();
                return hash;
            }
        }
    }

}

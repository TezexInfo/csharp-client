/* 
 * TezosAPI
 *
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Info.Tezex.Model
{
    /// <summary>
    /// Block
    /// </summary>
    [DataContract]
    public partial class Block :  IEquatable<Block>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Block" /> class.
        /// </summary>
        /// <param name="Hash">Hash.</param>
        /// <param name="NetId">NetId.</param>
        /// <param name="Protocol">Protocol.</param>
        /// <param name="Level">Level.</param>
        /// <param name="Proto">Proto.</param>
        /// <param name="Successors">Successors.</param>
        /// <param name="Predecessor">Predecessor.</param>
        /// <param name="Time">Time.</param>
        /// <param name="ValidationPass">ValidationPass.</param>
        /// <param name="Data">Data.</param>
        /// <param name="ChainStatus">ChainStatus.</param>
        /// <param name="OperationsCount">OperationsCount.</param>
        /// <param name="OperationsHash">OperationsHash.</param>
        /// <param name="Baker">Baker.</param>
        /// <param name="SeedNonceHash">SeedNonceHash.</param>
        /// <param name="ProofOfWorkNonce">ProofOfWorkNonce.</param>
        /// <param name="Signature">Signature.</param>
        /// <param name="Priority">Priority.</param>
        /// <param name="OperationCount">OperationCount.</param>
        /// <param name="TotalFee">TotalFee.</param>
        /// <param name="Operations">Operations.</param>
        public Block(string Hash = null, string NetId = null, string Protocol = null, int? Level = null, string Proto = null, List<ChainStatus> Successors = null, string Predecessor = null, DateTime? Time = null, string ValidationPass = null, string Data = null, string ChainStatus = null, int? OperationsCount = null, string OperationsHash = null, string Baker = null, string SeedNonceHash = null, string ProofOfWorkNonce = null, string Signature = null, int? Priority = null, int? OperationCount = null, string TotalFee = null, BlockOperationsSorted Operations = null)
        {
            this.Hash = Hash;
            this.NetId = NetId;
            this.Protocol = Protocol;
            this.Level = Level;
            this.Proto = Proto;
            this.Successors = Successors;
            this.Predecessor = Predecessor;
            this.Time = Time;
            this.ValidationPass = ValidationPass;
            this.Data = Data;
            this.ChainStatus = ChainStatus;
            this.OperationsCount = OperationsCount;
            this.OperationsHash = OperationsHash;
            this.Baker = Baker;
            this.SeedNonceHash = SeedNonceHash;
            this.ProofOfWorkNonce = ProofOfWorkNonce;
            this.Signature = Signature;
            this.Priority = Priority;
            this.OperationCount = OperationCount;
            this.TotalFee = TotalFee;
            this.Operations = Operations;
        }
        
        /// <summary>
        /// Gets or Sets Hash
        /// </summary>
        [DataMember(Name="hash", EmitDefaultValue=false)]
        public string Hash { get; set; }
        /// <summary>
        /// Gets or Sets NetId
        /// </summary>
        [DataMember(Name="net_id", EmitDefaultValue=false)]
        public string NetId { get; set; }
        /// <summary>
        /// Gets or Sets Protocol
        /// </summary>
        [DataMember(Name="protocol", EmitDefaultValue=false)]
        public string Protocol { get; set; }
        /// <summary>
        /// Gets or Sets Level
        /// </summary>
        [DataMember(Name="level", EmitDefaultValue=false)]
        public int? Level { get; set; }
        /// <summary>
        /// Gets or Sets Proto
        /// </summary>
        [DataMember(Name="proto", EmitDefaultValue=false)]
        public string Proto { get; set; }
        /// <summary>
        /// Gets or Sets Successors
        /// </summary>
        [DataMember(Name="successors", EmitDefaultValue=false)]
        public List<ChainStatus> Successors { get; set; }
        /// <summary>
        /// Gets or Sets Predecessor
        /// </summary>
        [DataMember(Name="predecessor", EmitDefaultValue=false)]
        public string Predecessor { get; set; }
        /// <summary>
        /// Gets or Sets Time
        /// </summary>
        [DataMember(Name="time", EmitDefaultValue=false)]
        public DateTime? Time { get; set; }
        /// <summary>
        /// Gets or Sets ValidationPass
        /// </summary>
        [DataMember(Name="validation_pass", EmitDefaultValue=false)]
        public string ValidationPass { get; set; }
        /// <summary>
        /// Gets or Sets Data
        /// </summary>
        [DataMember(Name="data", EmitDefaultValue=false)]
        public string Data { get; set; }
        /// <summary>
        /// Gets or Sets ChainStatus
        /// </summary>
        [DataMember(Name="chain_status", EmitDefaultValue=false)]
        public string ChainStatus { get; set; }
        /// <summary>
        /// Gets or Sets OperationsCount
        /// </summary>
        [DataMember(Name="operations_count", EmitDefaultValue=false)]
        public int? OperationsCount { get; set; }
        /// <summary>
        /// Gets or Sets OperationsHash
        /// </summary>
        [DataMember(Name="operations_hash", EmitDefaultValue=false)]
        public string OperationsHash { get; set; }
        /// <summary>
        /// Gets or Sets Baker
        /// </summary>
        [DataMember(Name="baker", EmitDefaultValue=false)]
        public string Baker { get; set; }
        /// <summary>
        /// Gets or Sets SeedNonceHash
        /// </summary>
        [DataMember(Name="seed_nonce_hash", EmitDefaultValue=false)]
        public string SeedNonceHash { get; set; }
        /// <summary>
        /// Gets or Sets ProofOfWorkNonce
        /// </summary>
        [DataMember(Name="proof_of_work_nonce", EmitDefaultValue=false)]
        public string ProofOfWorkNonce { get; set; }
        /// <summary>
        /// Gets or Sets Signature
        /// </summary>
        [DataMember(Name="signature", EmitDefaultValue=false)]
        public string Signature { get; set; }
        /// <summary>
        /// Gets or Sets Priority
        /// </summary>
        [DataMember(Name="priority", EmitDefaultValue=false)]
        public int? Priority { get; set; }
        /// <summary>
        /// Gets or Sets OperationCount
        /// </summary>
        [DataMember(Name="operation_count", EmitDefaultValue=false)]
        public int? OperationCount { get; set; }
        /// <summary>
        /// Gets or Sets TotalFee
        /// </summary>
        [DataMember(Name="total_fee", EmitDefaultValue=false)]
        public string TotalFee { get; set; }
        /// <summary>
        /// Gets or Sets Operations
        /// </summary>
        [DataMember(Name="operations", EmitDefaultValue=false)]
        public BlockOperationsSorted Operations { get; set; }
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Block {\n");
            sb.Append("  Hash: ").Append(Hash).Append("\n");
            sb.Append("  NetId: ").Append(NetId).Append("\n");
            sb.Append("  Protocol: ").Append(Protocol).Append("\n");
            sb.Append("  Level: ").Append(Level).Append("\n");
            sb.Append("  Proto: ").Append(Proto).Append("\n");
            sb.Append("  Successors: ").Append(Successors).Append("\n");
            sb.Append("  Predecessor: ").Append(Predecessor).Append("\n");
            sb.Append("  Time: ").Append(Time).Append("\n");
            sb.Append("  ValidationPass: ").Append(ValidationPass).Append("\n");
            sb.Append("  Data: ").Append(Data).Append("\n");
            sb.Append("  ChainStatus: ").Append(ChainStatus).Append("\n");
            sb.Append("  OperationsCount: ").Append(OperationsCount).Append("\n");
            sb.Append("  OperationsHash: ").Append(OperationsHash).Append("\n");
            sb.Append("  Baker: ").Append(Baker).Append("\n");
            sb.Append("  SeedNonceHash: ").Append(SeedNonceHash).Append("\n");
            sb.Append("  ProofOfWorkNonce: ").Append(ProofOfWorkNonce).Append("\n");
            sb.Append("  Signature: ").Append(Signature).Append("\n");
            sb.Append("  Priority: ").Append(Priority).Append("\n");
            sb.Append("  OperationCount: ").Append(OperationCount).Append("\n");
            sb.Append("  TotalFee: ").Append(TotalFee).Append("\n");
            sb.Append("  Operations: ").Append(Operations).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as Block);
        }

        /// <summary>
        /// Returns true if Block instances are equal
        /// </summary>
        /// <param name="other">Instance of Block to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Block other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return 
                (
                    this.Hash == other.Hash ||
                    this.Hash != null &&
                    this.Hash.Equals(other.Hash)
                ) && 
                (
                    this.NetId == other.NetId ||
                    this.NetId != null &&
                    this.NetId.Equals(other.NetId)
                ) && 
                (
                    this.Protocol == other.Protocol ||
                    this.Protocol != null &&
                    this.Protocol.Equals(other.Protocol)
                ) && 
                (
                    this.Level == other.Level ||
                    this.Level != null &&
                    this.Level.Equals(other.Level)
                ) && 
                (
                    this.Proto == other.Proto ||
                    this.Proto != null &&
                    this.Proto.Equals(other.Proto)
                ) && 
                (
                    this.Successors == other.Successors ||
                    this.Successors != null &&
                    this.Successors.SequenceEqual(other.Successors)
                ) && 
                (
                    this.Predecessor == other.Predecessor ||
                    this.Predecessor != null &&
                    this.Predecessor.Equals(other.Predecessor)
                ) && 
                (
                    this.Time == other.Time ||
                    this.Time != null &&
                    this.Time.Equals(other.Time)
                ) && 
                (
                    this.ValidationPass == other.ValidationPass ||
                    this.ValidationPass != null &&
                    this.ValidationPass.Equals(other.ValidationPass)
                ) && 
                (
                    this.Data == other.Data ||
                    this.Data != null &&
                    this.Data.Equals(other.Data)
                ) && 
                (
                    this.ChainStatus == other.ChainStatus ||
                    this.ChainStatus != null &&
                    this.ChainStatus.Equals(other.ChainStatus)
                ) && 
                (
                    this.OperationsCount == other.OperationsCount ||
                    this.OperationsCount != null &&
                    this.OperationsCount.Equals(other.OperationsCount)
                ) && 
                (
                    this.OperationsHash == other.OperationsHash ||
                    this.OperationsHash != null &&
                    this.OperationsHash.Equals(other.OperationsHash)
                ) && 
                (
                    this.Baker == other.Baker ||
                    this.Baker != null &&
                    this.Baker.Equals(other.Baker)
                ) && 
                (
                    this.SeedNonceHash == other.SeedNonceHash ||
                    this.SeedNonceHash != null &&
                    this.SeedNonceHash.Equals(other.SeedNonceHash)
                ) && 
                (
                    this.ProofOfWorkNonce == other.ProofOfWorkNonce ||
                    this.ProofOfWorkNonce != null &&
                    this.ProofOfWorkNonce.Equals(other.ProofOfWorkNonce)
                ) && 
                (
                    this.Signature == other.Signature ||
                    this.Signature != null &&
                    this.Signature.Equals(other.Signature)
                ) && 
                (
                    this.Priority == other.Priority ||
                    this.Priority != null &&
                    this.Priority.Equals(other.Priority)
                ) && 
                (
                    this.OperationCount == other.OperationCount ||
                    this.OperationCount != null &&
                    this.OperationCount.Equals(other.OperationCount)
                ) && 
                (
                    this.TotalFee == other.TotalFee ||
                    this.TotalFee != null &&
                    this.TotalFee.Equals(other.TotalFee)
                ) && 
                (
                    this.Operations == other.Operations ||
                    this.Operations != null &&
                    this.Operations.Equals(other.Operations)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.Hash != null)
                    hash = hash * 59 + this.Hash.GetHashCode();
                if (this.NetId != null)
                    hash = hash * 59 + this.NetId.GetHashCode();
                if (this.Protocol != null)
                    hash = hash * 59 + this.Protocol.GetHashCode();
                if (this.Level != null)
                    hash = hash * 59 + this.Level.GetHashCode();
                if (this.Proto != null)
                    hash = hash * 59 + this.Proto.GetHashCode();
                if (this.Successors != null)
                    hash = hash * 59 + this.Successors.GetHashCode();
                if (this.Predecessor != null)
                    hash = hash * 59 + this.Predecessor.GetHashCode();
                if (this.Time != null)
                    hash = hash * 59 + this.Time.GetHashCode();
                if (this.ValidationPass != null)
                    hash = hash * 59 + this.ValidationPass.GetHashCode();
                if (this.Data != null)
                    hash = hash * 59 + this.Data.GetHashCode();
                if (this.ChainStatus != null)
                    hash = hash * 59 + this.ChainStatus.GetHashCode();
                if (this.OperationsCount != null)
                    hash = hash * 59 + this.OperationsCount.GetHashCode();
                if (this.OperationsHash != null)
                    hash = hash * 59 + this.OperationsHash.GetHashCode();
                if (this.Baker != null)
                    hash = hash * 59 + this.Baker.GetHashCode();
                if (this.SeedNonceHash != null)
                    hash = hash * 59 + this.SeedNonceHash.GetHashCode();
                if (this.ProofOfWorkNonce != null)
                    hash = hash * 59 + this.ProofOfWorkNonce.GetHashCode();
                if (this.Signature != null)
                    hash = hash * 59 + this.Signature.GetHashCode();
                if (this.Priority != null)
                    hash = hash * 59 + this.Priority.GetHashCode();
                if (this.OperationCount != null)
                    hash = hash * 59 + this.OperationCount.GetHashCode();
                if (this.TotalFee != null)
                    hash = hash * 59 + this.TotalFee.GetHashCode();
                if (this.Operations != null)
                    hash = hash * 59 + this.Operations.GetHashCode();
                return hash;
            }
        }
    }

}

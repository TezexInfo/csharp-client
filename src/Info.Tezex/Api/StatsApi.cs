/* 
 * TezosAPI
 *
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RestSharp;
using Info.Tezex.Client;
using Info.Tezex.Model;

namespace Info.Tezex.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IStatsApi : IApiAccessor
    {
        #region Synchronous Operations
        /// <summary>
        /// Get Statistics
        /// </summary>
        /// <remarks>
        /// Get Statistics
        /// </remarks>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="group">Block, Transaction, etc</param>
        /// <param name="stat"></param>
        /// <param name="period"></param>
        /// <param name="startTime"> (optional)</param>
        /// <param name="endTime"> (optional)</param>
        /// <returns>List&lt;Stats&gt;</returns>
        List<Stats> GetStatistics (string group, string stat, string period, DateTime? startTime = null, DateTime? endTime = null);

        /// <summary>
        /// Get Statistics
        /// </summary>
        /// <remarks>
        /// Get Statistics
        /// </remarks>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="group">Block, Transaction, etc</param>
        /// <param name="stat"></param>
        /// <param name="period"></param>
        /// <param name="startTime"> (optional)</param>
        /// <param name="endTime"> (optional)</param>
        /// <returns>ApiResponse of List&lt;Stats&gt;</returns>
        ApiResponse<List<Stats>> GetStatisticsWithHttpInfo (string group, string stat, string period, DateTime? startTime = null, DateTime? endTime = null);
        /// <summary>
        /// Returns some basic Info
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>StatsOverview</returns>
        StatsOverview GetStatsOverview ();

        /// <summary>
        /// Returns some basic Info
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of StatsOverview</returns>
        ApiResponse<StatsOverview> GetStatsOverviewWithHttpInfo ();
        #endregion Synchronous Operations
        #region Asynchronous Operations
        /// <summary>
        /// Get Statistics
        /// </summary>
        /// <remarks>
        /// Get Statistics
        /// </remarks>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="group">Block, Transaction, etc</param>
        /// <param name="stat"></param>
        /// <param name="period"></param>
        /// <param name="startTime"> (optional)</param>
        /// <param name="endTime"> (optional)</param>
        /// <returns>Task of List&lt;Stats&gt;</returns>
        System.Threading.Tasks.Task<List<Stats>> GetStatisticsAsync (string group, string stat, string period, DateTime? startTime = null, DateTime? endTime = null);

        /// <summary>
        /// Get Statistics
        /// </summary>
        /// <remarks>
        /// Get Statistics
        /// </remarks>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="group">Block, Transaction, etc</param>
        /// <param name="stat"></param>
        /// <param name="period"></param>
        /// <param name="startTime"> (optional)</param>
        /// <param name="endTime"> (optional)</param>
        /// <returns>Task of ApiResponse (List&lt;Stats&gt;)</returns>
        System.Threading.Tasks.Task<ApiResponse<List<Stats>>> GetStatisticsAsyncWithHttpInfo (string group, string stat, string period, DateTime? startTime = null, DateTime? endTime = null);
        /// <summary>
        /// Returns some basic Info
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of StatsOverview</returns>
        System.Threading.Tasks.Task<StatsOverview> GetStatsOverviewAsync ();

        /// <summary>
        /// Returns some basic Info
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (StatsOverview)</returns>
        System.Threading.Tasks.Task<ApiResponse<StatsOverview>> GetStatsOverviewAsyncWithHttpInfo ();
        #endregion Asynchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class StatsApi : IStatsApi
    {
        private Info.Tezex.Client.ExceptionFactory _exceptionFactory = (name, response) => null;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatsApi"/> class.
        /// </summary>
        /// <returns></returns>
        public StatsApi(String basePath)
        {
            this.Configuration = new Configuration(new ApiClient(basePath));

            ExceptionFactory = Info.Tezex.Client.Configuration.DefaultExceptionFactory;

            // ensure API client has configuration ready
            if (Configuration.ApiClient.Configuration == null)
            {
                this.Configuration.ApiClient.Configuration = this.Configuration;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatsApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public StatsApi(Configuration configuration = null)
        {
            if (configuration == null) // use the default one in Configuration
                this.Configuration = Configuration.Default;
            else
                this.Configuration = configuration;

            ExceptionFactory = Info.Tezex.Client.Configuration.DefaultExceptionFactory;

            // ensure API client has configuration ready
            if (Configuration.ApiClient.Configuration == null)
            {
                this.Configuration.ApiClient.Configuration = this.Configuration;
            }
        }

        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        public String GetBasePath()
        {
            return this.Configuration.ApiClient.RestClient.BaseUrl.ToString();
        }

        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        [Obsolete("SetBasePath is deprecated, please do 'Configuration.ApiClient = new ApiClient(\"http://new-path\")' instead.")]
        public void SetBasePath(String basePath)
        {
            // do nothing
        }

        /// <summary>
        /// Gets or sets the configuration object
        /// </summary>
        /// <value>An instance of the Configuration</value>
        public Configuration Configuration {get; set;}

        /// <summary>
        /// Provides a factory method hook for the creation of exceptions.
        /// </summary>
        public Info.Tezex.Client.ExceptionFactory ExceptionFactory
        {
            get
            {
                if (_exceptionFactory != null && _exceptionFactory.GetInvocationList().Length > 1)
                {
                    throw new InvalidOperationException("Multicast delegate for ExceptionFactory is unsupported.");
                }
                return _exceptionFactory;
            }
            set { _exceptionFactory = value; }
        }

        /// <summary>
        /// Gets the default header.
        /// </summary>
        /// <returns>Dictionary of HTTP header</returns>
        [Obsolete("DefaultHeader is deprecated, please use Configuration.DefaultHeader instead.")]
        public Dictionary<String, String> DefaultHeader()
        {
            return this.Configuration.DefaultHeader;
        }

        /// <summary>
        /// Add default header.
        /// </summary>
        /// <param name="key">Header field name.</param>
        /// <param name="value">Header field value.</param>
        /// <returns></returns>
        [Obsolete("AddDefaultHeader is deprecated, please use Configuration.AddDefaultHeader instead.")]
        public void AddDefaultHeader(string key, string value)
        {
            this.Configuration.AddDefaultHeader(key, value);
        }

        /// <summary>
        /// Get Statistics Get Statistics
        /// </summary>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="group">Block, Transaction, etc</param>
        /// <param name="stat"></param>
        /// <param name="period"></param>
        /// <param name="startTime"> (optional)</param>
        /// <param name="endTime"> (optional)</param>
        /// <returns>List&lt;Stats&gt;</returns>
        public List<Stats> GetStatistics (string group, string stat, string period, DateTime? startTime = null, DateTime? endTime = null)
        {
             ApiResponse<List<Stats>> localVarResponse = GetStatisticsWithHttpInfo(group, stat, period, startTime, endTime);
             return localVarResponse.Data;
        }

        /// <summary>
        /// Get Statistics Get Statistics
        /// </summary>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="group">Block, Transaction, etc</param>
        /// <param name="stat"></param>
        /// <param name="period"></param>
        /// <param name="startTime"> (optional)</param>
        /// <param name="endTime"> (optional)</param>
        /// <returns>ApiResponse of List&lt;Stats&gt;</returns>
        public ApiResponse< List<Stats> > GetStatisticsWithHttpInfo (string group, string stat, string period, DateTime? startTime = null, DateTime? endTime = null)
        {
            // verify the required parameter 'group' is set
            if (group == null)
                throw new ApiException(400, "Missing required parameter 'group' when calling StatsApi->GetStatistics");
            // verify the required parameter 'stat' is set
            if (stat == null)
                throw new ApiException(400, "Missing required parameter 'stat' when calling StatsApi->GetStatistics");
            // verify the required parameter 'period' is set
            if (period == null)
                throw new ApiException(400, "Missing required parameter 'period' when calling StatsApi->GetStatistics");

            var localVarPath = "/stats/{group}/{stat}/{period}";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new Dictionary<String, String>();
            var localVarHeaderParams = new Dictionary<String, String>(Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
                "application/json", 
                "application/xml"
            };
            String localVarHttpContentType = Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/xml", 
                "application/json"
            };
            String localVarHttpHeaderAccept = Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            // set "format" to json by default
            // e.g. /pet/{petId}.{format} becomes /pet/{petId}.json
            localVarPathParams.Add("format", "json");
            if (group != null) localVarPathParams.Add("group", Configuration.ApiClient.ParameterToString(group)); // path parameter
            if (stat != null) localVarPathParams.Add("stat", Configuration.ApiClient.ParameterToString(stat)); // path parameter
            if (period != null) localVarPathParams.Add("period", Configuration.ApiClient.ParameterToString(period)); // path parameter
            if (startTime != null) localVarQueryParams.Add("start_time", Configuration.ApiClient.ParameterToString(startTime)); // query parameter
            if (endTime != null) localVarQueryParams.Add("end_time", Configuration.ApiClient.ParameterToString(endTime)); // query parameter


            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) Configuration.ApiClient.CallApi(localVarPath,
                Method.GET, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("GetStatistics", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<List<Stats>>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                (List<Stats>) Configuration.ApiClient.Deserialize(localVarResponse, typeof(List<Stats>)));
            
        }

        /// <summary>
        /// Get Statistics Get Statistics
        /// </summary>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="group">Block, Transaction, etc</param>
        /// <param name="stat"></param>
        /// <param name="period"></param>
        /// <param name="startTime"> (optional)</param>
        /// <param name="endTime"> (optional)</param>
        /// <returns>Task of List&lt;Stats&gt;</returns>
        public async System.Threading.Tasks.Task<List<Stats>> GetStatisticsAsync (string group, string stat, string period, DateTime? startTime = null, DateTime? endTime = null)
        {
             ApiResponse<List<Stats>> localVarResponse = await GetStatisticsAsyncWithHttpInfo(group, stat, period, startTime, endTime);
             return localVarResponse.Data;

        }

        /// <summary>
        /// Get Statistics Get Statistics
        /// </summary>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="group">Block, Transaction, etc</param>
        /// <param name="stat"></param>
        /// <param name="period"></param>
        /// <param name="startTime"> (optional)</param>
        /// <param name="endTime"> (optional)</param>
        /// <returns>Task of ApiResponse (List&lt;Stats&gt;)</returns>
        public async System.Threading.Tasks.Task<ApiResponse<List<Stats>>> GetStatisticsAsyncWithHttpInfo (string group, string stat, string period, DateTime? startTime = null, DateTime? endTime = null)
        {
            // verify the required parameter 'group' is set
            if (group == null)
                throw new ApiException(400, "Missing required parameter 'group' when calling StatsApi->GetStatistics");
            // verify the required parameter 'stat' is set
            if (stat == null)
                throw new ApiException(400, "Missing required parameter 'stat' when calling StatsApi->GetStatistics");
            // verify the required parameter 'period' is set
            if (period == null)
                throw new ApiException(400, "Missing required parameter 'period' when calling StatsApi->GetStatistics");

            var localVarPath = "/stats/{group}/{stat}/{period}";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new Dictionary<String, String>();
            var localVarHeaderParams = new Dictionary<String, String>(Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
                "application/json", 
                "application/xml"
            };
            String localVarHttpContentType = Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/xml", 
                "application/json"
            };
            String localVarHttpHeaderAccept = Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            // set "format" to json by default
            // e.g. /pet/{petId}.{format} becomes /pet/{petId}.json
            localVarPathParams.Add("format", "json");
            if (group != null) localVarPathParams.Add("group", Configuration.ApiClient.ParameterToString(group)); // path parameter
            if (stat != null) localVarPathParams.Add("stat", Configuration.ApiClient.ParameterToString(stat)); // path parameter
            if (period != null) localVarPathParams.Add("period", Configuration.ApiClient.ParameterToString(period)); // path parameter
            if (startTime != null) localVarQueryParams.Add("start_time", Configuration.ApiClient.ParameterToString(startTime)); // query parameter
            if (endTime != null) localVarQueryParams.Add("end_time", Configuration.ApiClient.ParameterToString(endTime)); // query parameter


            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) await Configuration.ApiClient.CallApiAsync(localVarPath,
                Method.GET, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("GetStatistics", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<List<Stats>>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                (List<Stats>) Configuration.ApiClient.Deserialize(localVarResponse, typeof(List<Stats>)));
            
        }

        /// <summary>
        /// Returns some basic Info 
        /// </summary>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>StatsOverview</returns>
        public StatsOverview GetStatsOverview ()
        {
             ApiResponse<StatsOverview> localVarResponse = GetStatsOverviewWithHttpInfo();
             return localVarResponse.Data;
        }

        /// <summary>
        /// Returns some basic Info 
        /// </summary>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of StatsOverview</returns>
        public ApiResponse< StatsOverview > GetStatsOverviewWithHttpInfo ()
        {

            var localVarPath = "/stats/overview";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new Dictionary<String, String>();
            var localVarHeaderParams = new Dictionary<String, String>(Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
                "application/json", 
                "application/xml"
            };
            String localVarHttpContentType = Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/xml", 
                "application/json"
            };
            String localVarHttpHeaderAccept = Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            // set "format" to json by default
            // e.g. /pet/{petId}.{format} becomes /pet/{petId}.json
            localVarPathParams.Add("format", "json");


            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) Configuration.ApiClient.CallApi(localVarPath,
                Method.GET, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("GetStatsOverview", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<StatsOverview>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                (StatsOverview) Configuration.ApiClient.Deserialize(localVarResponse, typeof(StatsOverview)));
            
        }

        /// <summary>
        /// Returns some basic Info 
        /// </summary>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of StatsOverview</returns>
        public async System.Threading.Tasks.Task<StatsOverview> GetStatsOverviewAsync ()
        {
             ApiResponse<StatsOverview> localVarResponse = await GetStatsOverviewAsyncWithHttpInfo();
             return localVarResponse.Data;

        }

        /// <summary>
        /// Returns some basic Info 
        /// </summary>
        /// <exception cref="Info.Tezex.Client.ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (StatsOverview)</returns>
        public async System.Threading.Tasks.Task<ApiResponse<StatsOverview>> GetStatsOverviewAsyncWithHttpInfo ()
        {

            var localVarPath = "/stats/overview";
            var localVarPathParams = new Dictionary<String, String>();
            var localVarQueryParams = new Dictionary<String, String>();
            var localVarHeaderParams = new Dictionary<String, String>(Configuration.DefaultHeader);
            var localVarFormParams = new Dictionary<String, String>();
            var localVarFileParams = new Dictionary<String, FileParameter>();
            Object localVarPostBody = null;

            // to determine the Content-Type header
            String[] localVarHttpContentTypes = new String[] {
                "application/json", 
                "application/xml"
            };
            String localVarHttpContentType = Configuration.ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            String[] localVarHttpHeaderAccepts = new String[] {
                "application/xml", 
                "application/json"
            };
            String localVarHttpHeaderAccept = Configuration.ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
                localVarHeaderParams.Add("Accept", localVarHttpHeaderAccept);

            // set "format" to json by default
            // e.g. /pet/{petId}.{format} becomes /pet/{petId}.json
            localVarPathParams.Add("format", "json");


            // make the HTTP request
            IRestResponse localVarResponse = (IRestResponse) await Configuration.ApiClient.CallApiAsync(localVarPath,
                Method.GET, localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarFileParams,
                localVarPathParams, localVarHttpContentType);

            int localVarStatusCode = (int) localVarResponse.StatusCode;

            if (ExceptionFactory != null)
            {
                Exception exception = ExceptionFactory("GetStatsOverview", localVarResponse);
                if (exception != null) throw exception;
            }

            return new ApiResponse<StatsOverview>(localVarStatusCode,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                (StatsOverview) Configuration.ApiClient.Deserialize(localVarResponse, typeof(StatsOverview)));
            
        }

    }
}

/* 
 * TezosAPI
 *
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Info.Tezex.Api;
using Info.Tezex.Model;
using Info.Tezex.Client;
using System.Reflection;

namespace Info.Tezex.Test
{
    /// <summary>
    ///  Class for testing Level
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class LevelTests
    {
        // TODO uncomment below to declare an instance variable for Level
        //private Level instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of Level
            //instance = new Level();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of Level
        /// </summary>
        [Test]
        public void LevelInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" Level
            //Assert.IsInstanceOfType<Level> (instance, "variable 'instance' is a Level");
        }

        /// <summary>
        /// Test the property '_Level'
        /// </summary>
        [Test]
        public void _LevelTest()
        {
            // TODO unit test for the property '_Level'
        }

    }

}

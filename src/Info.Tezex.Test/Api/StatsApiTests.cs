/* 
 * TezosAPI
 *
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using Info.Tezex.Client;
using Info.Tezex.Api;
using Info.Tezex.Model;

namespace Info.Tezex.Test
{
    /// <summary>
    ///  Class for testing StatsApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class StatsApiTests
    {
        private StatsApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new StatsApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of StatsApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' StatsApi
            //Assert.IsInstanceOfType(typeof(StatsApi), instance, "instance is a StatsApi");
        }

        
        /// <summary>
        /// Test GetStatistics
        /// </summary>
        [Test]
        public void GetStatisticsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string group = null;
            //string stat = null;
            //string period = null;
            //DateTime? startTime = null;
            //DateTime? endTime = null;
            //var response = instance.GetStatistics(group, stat, period, startTime, endTime);
            //Assert.IsInstanceOf<List<Stats>> (response, "response is List<Stats>");
        }
        
        /// <summary>
        /// Test GetStatsOverview
        /// </summary>
        [Test]
        public void GetStatsOverviewTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //var response = instance.GetStatsOverview();
            //Assert.IsInstanceOf<StatsOverview> (response, "response is StatsOverview");
        }
        
    }

}
